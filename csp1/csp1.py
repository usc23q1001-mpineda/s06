from abc import ABC, abstractclassmethod

class Person(ABC):
    
 @abstractclassmethod
 def getFullName(self):
  pass
    
 def addRequest(self):
  pass

 def checkRequest(self):
  pass

 def addUser(self):
  pass

class Employee(Person):
    def __init__(self,firstName,lastName,email,department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        
    #Setters
    def set_firstName(self, firstName):
        self._firstName = firstName

    def set_lastName(self, lastName):
        self._lastName = lastName

    def set_email(self, email):
        self._email = email
    
    def set_department(self, department):
        self._department = department
        
    #Getters
    def get_firstName(self):
        print(f"Employee's firstName: {self._firstName}.")

    def get_lastName(self):
        print(f"Employee's lastName {self._lastName}.")

    def get_email(self):
        print(f"Employee's email: {self._email}")
        
    def get_department(self):
        print(f"Employee's department: {self._department}")
   
    #Abstract Methods
    def getFullName(self):
        return f'{self._firstName} {self._lastName}'
    
    def addRequest(self):
        return f'Request has been added'
    
    def checkRequest(self):
        print("Checking the request..")
    
    def addUser(self):
        pass
    
    def login(self):
        return f'{self._email} has logged in'
        
    def logout(self):
        return f'{self._email} has logged out'
   
        
class TeamLead(Person): 
    def __init__(self,firstName,lastName,email,department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []
        
    #Setters
    def set_firstName(self, firstName):
        self._firstName = firstName

    def set_lastName(self, lastName):
        self._lastName = lastName

    def set_email(self, email):
        self._email = email
    
    def set_department(self, department):
        self._department = department
        
    #Getters
    def get_firstName(self):
        print(f"Teamlead's firstName: {self._firstName}.")

    def get_lastName(self):
        print(f"Teamlead's lastName {self._lastName}.")

    def get_email(self):
        print(f"Teamlead's email: {self._email}")
        
    def get_department(self):
        print(f"Teamlead's department: {self._department}")
    
    def get_members(self):
        return self._members

    #Abstract Methods
    def getFullName(self):
        return f'{self._firstName} {self._lastName}'
    
    def checkRequest(self):
        pass
        
    def addRequest(self):
        return f'Request has been added'
    
    def addUser(self):
        pass
    
    def login(self):
        return f'{self._email} has logged in'
        
    def logout(self):
        return f'{self._email} has logged out'
    
    def addMember(self, employee):
        self._members.append(employee)
        
class Admin(Person): 
    def __init__(self,firstName,lastName,email,department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    
    #Setters
    def set_firstName(self, firstName):
        self._firstName = firstName

    def set_lastName(self, lastName):
        self._lastName = lastName

    def set_email(self, email):
        self._email = email
    
    def set_department(self, department):
        self._department = department
        
    #Getters
    def get_firstName(self):
        print(f"Admin's firstName: {self._firstName}.")

    def get_lastName(self):
        print(f"Admin's lastName {self._lastName}.")

    def get_email(self):
        print(f"Admin's email: {self._email}")
        
    def get_department(self):
        print(f"Admin's department: {self._department}")
        
    #Abstract Methods
    def getFullName(self):
        return f'{self._firstName} {self._lastName}'
    
    def checkRequest(self):
        print("Checking the request..")
    
    def addRequest(self):
        return f'Request has been added'
    
    def addUser(self):
        pass
    
    def login(self):
        return f'{self._email} has logged in'
        
    def logout(self):
        return f'{self._email} has logged out'
    
    def addUser(self):
        return f'User has been added'
        
class Request():
    def __init__(self, name, requester, dateRequested, status):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = status
    
    #Setters
    def set_name(self, name):
        self._name = name
        
    def set_requester(self, requester):
        self._requester = requester
        
    def set_dateRequested(self, dateRequested):
        self.dateRequested = dateRequested
        
    def set_status(self, status):
        self._status = status

    #Getters
    def get_name(self):
        return self._name
    
    def get_requester(self):
        return self._requester
    
    def get_dateRequested(self):
        return self._dateRequested
    
    def get_status(self):
        return self._status

    #Methods
    def updateRequest(self):
        pass
        
    def closeRequest(self):
        return f'Request {self._name} has been closed'
  
    def cancelRequest(self):
        pass
        
        
        


#Test Cases
employee1 = Employee("John", "Doe", "djohn@gmail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@gmail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@gmail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@gmail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@gmail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@gmail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021", "Ongoing")
req2 = Request("Laptop repair", employee1, "1-Jul-2021", "Ongoing")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@gmail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@gmail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
